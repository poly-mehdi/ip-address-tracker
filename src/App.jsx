import iconArrow from './images/icon-arrow.svg'
import { useEffect, useState, useRef } from 'react'
import { MapContainer, TileLayer, useMap, Marker, Popup } from 'react-leaflet'
import iconLocation from './images/icon-location.svg'
import 'leaflet/dist/leaflet.css'

const App = () => {
  // Constants
  const [ipAddress, setIpAddress] = useState('')
  const [ownIp, setOwnIp] = useState('')
  const [ipSearch, setIpSearch] = useState('')
  const [location, setLocation] = useState({
    latitude: 0,
    longitude: 0,
    info: '',
    timezone: '',
    isp: '',
  })
  const customIcon = L.icon({
    iconUrl: iconLocation,
    iconSize: [46, 56],
    iconAnchor: [15, 40],
  })
  // useRef
  const mapRef = useRef(null)

  // Functions
  const fetchLocation = async (ip) => {
    try {
      const locationSearch = await fetch(
        `https://geo.ipify.org/api/v2/country,city?apiKey=${
          import.meta.env.VITE_IP_KEY
        }&ipAddress=${ip}`
      )
      const response = await locationSearch.json()
      setLocation({
        latitude: response.location.lat,
        longitude: response.location.lng,
        info: `${response.location.city}, ${response.location.country} ${response.location.postalCode}`,
        timezone: `UTC${response.location.timezone}`,
        isp: response.isp,
      })
    } catch (error) {
      console.log('There was an error ' + error)
    }
  }

  const handleSubmit = (e) => {
    e.preventDefault()
    fetchLocation(ipSearch)
    setIpAddress(ipSearch)
  }

  // UseEffects
  useEffect(() => {
    const fetchUserIp = async () => {
      try {
        const ipUser = await fetch('https://api.ipify.org/?format=json')
        const response = await ipUser.json()
        setOwnIp(response.ip)
        setIpAddress(response.ip)
      } catch (error) {
        console.log(error)
      }
    }

    fetchUserIp()
  }, [])

  useEffect(() => {
    const fetchOwnLocation = async () => {
      try {
        const locationSearch = await fetch(
          `https://geo.ipify.org/api/v2/country,city?apiKey=${
            import.meta.env.VITE_IP_KEY
          }&ipAddress=${ipAddress}`
        )
        const response = await locationSearch.json()
        setLocation({
          latitude: response.location.lat,
          longitude: response.location.lng,
          info: `${response.location.city}, ${response.location.country} ${response.location.postalCode}`,
          timezone: `UTC${response.location.timezone}`,
          isp: response.isp,
        })
      } catch (error) {
        console.log('There was an error ' + error)
      }
    }

    if (ownIp) {
      fetchOwnLocation()
    }
  }, [ownIp, ipAddress])

  useEffect(() => {
    if (mapRef.current) {
      mapRef.current.panTo([location.latitude, location.longitude])
    }
  }, [location])

  return (
    <main>
      <header>
        <section className="header-container">
          <h1 className="header-title">IP Address Tracker</h1>
          <form className="header-form" onSubmit={handleSubmit}>
            <div className="input-container">
              <input
                type="text"
                placeholder="Search for any IP address or domain"
                value={ipSearch}
                onChange={(e) => {
                  setIpSearch(e.target.value)
                }}
              />
              <button className="header-btn" type="submit">
                <img src={iconArrow} alt="Submit" />
              </button>
            </div>
          </form>
          <div className="info-container">
            <div className="single-info">
              <p>IP Address</p>
              <h2>{ipAddress}</h2>
            </div>
            <div className="underline"></div>
            <div className="single-info">
              <p>Location</p>
              <h2>{location.info}</h2>
            </div>
            <div className="underline"></div>
            <div className="single-info">
              <p>Timezone</p>
              <h2>{location.timezone}</h2>
            </div>
            <div className="underline"></div>
            <div className="single-info">
              <p>ISP</p>
              <h2>{location.isp}</h2>
            </div>
          </div>
        </section>
      </header>
      <MapContainer
        ref={mapRef}
        center={[location.longitude, location.latitude]}
        zoom={14}
        scrollWheelZoom={false}
        zoomControl={false}
      >
        <TileLayer
          attribution='&copy; <a href="https://www.mapbox.com/about/maps/">Mapbox</a> contributors'
          url={`https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=${
            import.meta.env.VITE_MAPBOX_KEY
          }`}
          id="mapbox/streets-v11"
        />
        <Marker
          position={[location.latitude, location.longitude]}
          icon={customIcon}
        ></Marker>
      </MapContainer>
    </main>
  )
}

export default App
